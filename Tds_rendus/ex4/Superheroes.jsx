/* eslint-disable react/destructuring-assignment */
import React from 'react';

const Superheroes = (props) => (
  <div>
    <h1>{props.name}</h1>
  </div>
);

export default Superheroes;
